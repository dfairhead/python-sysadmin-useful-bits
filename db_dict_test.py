from db_dict import KeyVal

with KeyVal('test.db') as d:
    if 'test' in d:
        print 'd has test! it is: %s' %  d['test']
    d['test'] = 'stuff'
