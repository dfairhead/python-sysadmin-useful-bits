#!/usr/bin/python
# -*- coding: UTF-8 -*-

'''
A dict-like object (KeyVal) which stores it's keys & values to a sqlite3 database.
Usage:

>>> with KeyVal('stuff.db') as db:
>>>     if 'key' in db:
>>>         print 'Key is in the database!'
>>>         print 'The Value is:', db['key']

also works with other normal dictlike functions:

>>> del db['foo']
>>> for k in db:
        print db[k]

>>> for k,v in db.items():
>>>     do_stuff...

>>> db.update({'this':'is','quite':'efficient','to':'use'})

>>> len(db)
3 # number of records.

etc.

Also has the iter versions of the functions for your memory friendliness.

'''

import sys
reload(sys)
sys.setdefaultencoding('utf-8')

import sqlite3
from cPickle import dumps, loads, HIGHEST_PROTOCOL as PICKLE_PROTOCOL

def clean(unclean):
    ''' Makes a string safe (and unicode) to use as
    a column name in a SQLlite query '''
    return unicode(unclean.replace('"','""'))

def cleanq(unclean):
    ''' Cleans a string, and sticks quotes around it, for use in
    SQLlite queries. '''
    return u'"' + unicode(unclean.replace('"','""')) + u'"'

#############################
# cPickle encode/decode (for values)

def encode(obj):
    """Serialize an object using pickle to a binary format accepted by SQLite."""
    return sqlite3.Binary(dumps(obj, protocol=PICKLE_PROTOCOL))


def decode(obj):
    """Deserialize objects retrieved from SQLite."""
    return loads(str(obj))


class KeyVal(object):

    def __init__(self, db_name=":memory:", table_name=u"keyval"):
        self.db_name = db_name
        self.table_name = clean(table_name)

    def open(self):
        self.db = sqlite3.connect(self.db_name)
        self.cur = self.db.cursor()
        self._cache = {}

        self._exec(u'CREATE TABLE IF NOT EXISTS "{0}"'
                    '(Id INTEGER PRIMARY KEY, Key TEXT UNIQUE, Value BLOB);'.format(self.table_name))

        self._exec(u'CREATE TABLE IF NOT EXISTS "__KEYVAL_STATS__"'
                          '(Id INTEGER PRIMARY KEY, TableName TEXT UNIQUE, Size INTEGER);')

        # If we try to get the length of the table when it doesn't exist, it throws a wobbly.
        try:
            len(self)
        except TypeError:
            self._exec(u'INSERT INTO "__KEYVAL_STATS__"(TableName, Size) VALUES (?,?);',
                       (self.table_name, 0))

            # triggers for auto increasing and decreasing the size in the stats table.

            self._exec(u'CREATE TRIGGER "_increase_size_{0}_" AFTER INSERT ON "{0}" FOR EACH ROW '
                        'BEGIN'
                        '   UPDATE "__KEYVAL_STATS__" SET Size = Size + 1 WHERE TableName = "{0}";'
                        'END;'.format(self.table_name))

            self._exec(u'CREATE TRIGGER "_decrease_size_{0}_" AFTER DELETE ON "{0}" FOR EACH ROW '
                        'BEGIN'
                        '   UPDATE "__KEYVAL_STATS__" SET Size = Size - 1 WHERE TableName = "{0}";'
                        'END;'.format(self.table_name))

        self.db.commit()


    def _exec(self, *vargs, **kvargs):
        return self.cur.execute(*vargs, **kvargs)

    def close(self):
        self.db.commit()
        self.db.close()

    def __enter__(self):
        ''' open the database, using 'with' pattern '''
        self.open()
        return self

    def __exit__(self, exptype, expvalue, exptb):
        ''' close the database, using the 'with' pattern '''
        self.close()

    def __len__(self):
        self._exec(u'SELECT Size FROM "__KEYVAL_STATS__" WHERE TableName=?',(self.table_name,))

        return self.cur.fetchone()[0]

    def __getitem__(self, key):
        if key in self._cache:
            return self._cache[key]

        self._exec(u'SELECT Value FROM "{0}" WHERE Key=?;'.format(self.table_name), (key,))
        row = self.cur.fetchone()
        if row == None:
            raise KeyError('Not in DB')
        else:
            return decode(row[0])

    def __setitem__(self, key, value):

        self._exec(u'INSERT OR IGNORE INTO "{0}"(Key, Value) VALUES (?,?)'.format(self.table_name), (key, encode(value)))
        self._exec(u'UPDATE "{0}" SET Key=?, Value=? WHERE Key=?;'.format(self.table_name), (key, encode(value), key))

        self._cache[key] = value

    def __delitem__(self, key):
        self._exec(u'DELETE FROM "{0}" WHERE Key=?'.format(self.table_name), (key,))

        del self._cache[key]

    def __contains__(self, item):
        self._exec(u'SELECT Value FROM "{0}" WHERE KEY=?'.format(self.table_name), (item,))
        value = self.cur.fetchone()
        if value == None:
            return False
        else:
            self._cache[item] = decode(value[0])
            return True

    def clearcache(self):
        self._cache = {}

    # Various Iteration functions:

    def __iter__(self):
        # we need a local cursor for this, since we give up control during the SELECT...
        c = self.db.cursor()
        c.execute(u'SELECT Key FROM "{0}";'.format(self.table_name))
        while True:
            current_row = c.fetchone()
            if current_row == None:
                break

            yield current_row[0]

    def iteritems(self):
        c = self.db.cursor()
        c.execute(u'SELECT Key, Value FROM "{0}";'.format(self.table_name))
        while True:
            current_row = c.fetchone()
            if current_row == None:
                break
            yield current_row[0], decode(current_row[1])

    def items(self):
        self._exec(u'SELECT Key, Value FROM "{0}";'.format(self.table_name))
        return [(r[0],decode(r[1])) for r in self.cur.fetchall()]

    def keys(self):
        self._exec(u'SELECT Key FROM "{0}";'.format(self.table_name))
        return [r[0] for r in self.cur.fetchall()]

    def values(self):
        self._exec(u'SELECT Value FROM "{0}";'.format(self.table_name))
        return [decode(r[0]) for r in self.cur.fetchall()]

    def iterkeys(self):
        for k,v in self.iteritems():
            yield k

    def itervalues(self):
        for k,v in self.iteritems():
            yield v #already decoded by iteritems...

    # and other old python support...

    def has_key(self, k):
        return k in self

    def update(self, E, **F):
        ''' Copied from dict.update -
            D.update([E, ]**F) -> None.  Update D from dict/iterable E and F.
            If E present and has a .keys() method, does:     for k in E: D[k] = E[k]
            If E present and lacks .keys() method, does:     for (k, v) in E: D[k] = v
            In either case, this is followed by: for k in F: D[k] = F[k] '''
        if E:
            if hasattr(E, 'keys'):
                for k in E:
                    self[k] = E[k]
            else:
                for k,v in E:
                    self[k] = v
        for k in F:
            self[k] = F[k]

    def pop(self, k):
        v = self[k]
        del(self[k])
        return v

    def popitem(self):
        self._exec(u'SELECT Key,Value FROM "{0}" LIMIT 1;'.format(self.table_name))
        try:
            row = self.cur.fetchone()
            del (self[row[0]])
            return row[0], decode(row[1])
        except TypeError:
            raise KeyError('Empty Table!')
