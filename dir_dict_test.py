from os.path import isdir
from dir_dict import Dir, File, CD
from md5files import md5file

from multiprocessing import Pool

def clean_osx_crud(path, recursive=True):
    for p in Dir(path).files(recursive=recursive):
        if p.startswith['._']:
            p.delete()

def md5sum(filename):
    print('MD5(%s) = %s'.format(filename, md5file(filename.abspath))

def md5s(path):
    if isdir(path):
        for f in Dir(path).ifiles(recursive=True):
            md5sum(f.relpath(path))
    else:
        md5sum(path)

def md5sasync(path):
    pool = Pool(processes=4)
    pool.map(md5sum, 
