from dir_dict import Dir
from md5files import md5file
from os.path import relpath
from os import getcwd

def md5dir(path, rootdir):
    for d in path.dirs():
        md5dir(d, rootdir)

    for f in path.files():
        print 'MD5 (./%s) = %s'%(relpath(f.abspath, rootdir), md5file(f.abspath))


if __name__ == '__main__':
    md5dir(Dir('.'), getcwd())
