from dir_dict import Dir
from md5files import md5file
from os.path import relpath
from os import getcwd
from multiprocessing import Pool

rootdir = getcwd()

def do_file(f):
    print 'MD5 (./%s) = %s'%(relpath(f.abspath, rootdir), md5file(f.abspath))

def md5dir(path):
    for d in path.dirs():
        md5dir(d)

    pool = Pool(processes=4)
    pool.map(do_file, path.files())


if __name__ == '__main__':
    rootdir = getcwd()
    md5dir(Dir('.'))
