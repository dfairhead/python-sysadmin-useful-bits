#!/usr/bin/python

from hashlib import md5
from sys import argv
import gc

def md5file(filename, blocks=5000):
    md5sum = md5()
    bs = md5sum.block_size * blocks
    five = 5
    with open(filename,'rb') as f:
        while True:
            data = f.read(bs)
            if not data:
                break
            md5sum.update(data)
            if five == 0:
                gc.collect()
                five = 5
            else:
                five -= 1
    return md5sum.hexdigest()

if __name__ == '__main__':
    if len(argv) == 2:
        print 'MD5 (%s) = %s'%(argv[1], md5file(argv[1]))

