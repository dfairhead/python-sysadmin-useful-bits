#!/usr/bin/python
# -*- coding: UTF-8 -*-

import sys
reload(sys)
sys.setdefaultencoding('utf-8')
from os.path import lexists, isdir, isfile, islink, ismount, abspath, relpath, basename, dirname, join as pathjoin
from os import unlink, getcwd, chdir
from glob import glob, iglob
from multiprocessing import Pool

class CD():
    ''' class for changing to a directory just to run some actions,
        and automatically return when we leave.  use:
        >>> with CD('/tmp/'):
                touch 'file'
    '''
    def __init__(self, newpath):
        assert lexists(newpath)
        assert isdir(newpath)

        self.oldpath = getcwd()
        self.newpath = newpath

    def __enter__(self):
        chdir(self.newpath)

    def __exit__(self, exptype, expvalue, exptb):
        chdir(self.oldpath)

class _PathObject(unicode):
    ''' generic path object, for existant files/dirs/links/etc.  Only really
        to be used as a base class, not as real objects. '''
    def __init__(self, pathname):
        assert lexists(pathname)
        self.path = pathname
        self.abspath = abspath(pathname)
    def pstr(self):
        return '%s(%s)'%(self.__class__.__name__,self)

def _WhichPathObject(pathname, rootdir=''):
    ''' Try to work out what kind of object (File, Dir, etc) something is,
        and return an object of that kind. '''

    
    rel = relpath(pathname, rootdir)

    with CD(abspath(rootdir)):
        if not lexists(rel):
            raise IOError('Path %s does not exist (in: %s)' % \
                (rel, abspath(rootdir)))

        if isdir(rel):
            return Dir(rel)
        elif isfile(rel):
            return File(rel)
        elif islink(rel):
            return Link(rel)
        elif ismount(rel):
            return Mount(rel)
        else:
            return _PathObject(rel)


class Dir(_PathObject):
    def __init__(self, pathname):
        super(Dir, self).__init__(pathname)

        assert isdir(pathname)

        self.globallstr = pathjoin(self.abspath,'*')

    def __len__(self):
        return len(glob(self.globallstr))

    def __setitem__(self, key, value):
        # TODO... how should this work?
        raise TypeError('Sorry, I have not decided how yet to do this...')

    def __getitem__(self, key):
        with CD(self.abspath):
            if lexists(key):
                return _WhichPathObject(abspath(key), self)
            else:
                raise KeyError('File Does not exist!')

    def delete(self, key):
        with CD(self.abspath):
            assert lexists(key)

            #if isfile(key):
            #    unlink(key)
            #else:
            #    x = Dir(key)
            #    del x
            print 'TODO: DIR-actually delete things, once we are test-ready'

    def __contains__(self, key):
        return lexists(pathjoin(self.abspath, key))

    #########
    # Various Iteration functions:

    def __iter__(self):
        for path in iglob(pathjoin(self.abspath,'*')):
            yield(_WhichPathObject(path, self))

    def __call__(self, globstr='*'):
        with CD(self.abspath):
            return [_WhichPathObject(path, self) for path in iglob(globstr)]

    def files(self, recursive=False):
        with CD(self.abspath):
            return [File(f) for f in glob('*') if isfile(f)]

    def dirs(self, recursive=False):
        with CD(self.abspath):
            return [Dir(f) for f in glob('*') if isdir(f)]

    def ifiles(self, recursive=False):
        ''' iteratres through all the files of this Dir.  If recursive,
            then it will also yield all files in all subdirectories. Depth
            First Search for algorithmic simplicity. '''
        with CD(self.abspath):
            for path in iglob('*'):
                if isfile(path):
                    yield(File(path))
                if recursive and isdir(path):
                    for f in Dir(path).ifiles(recursive=True):
                        yield f

    def idirs(self, recursive=False):
        ''' iterates though all the subdirectories of this Dir.  If recursive,
            then recursively do the same through all child dirs.  Depth First
            Search for algorithmic simplicity. '''
        with CD(self.abspath):
            for path in iglob('*'):
                if isdir(path):
                    if recursive:
                        for p in Dir(path).idirs(recursive=True):
                            yield p
                    yield(Dir(path))


class _FileLike(_PathObject):
    ''' File like objects.  Can be files, symlinks, devices, etc. '''
    def __init__(self, pathname):
        assert lexists(pathname)
        assert not isdir(pathname)

        self.pathname = pathname
        self.abspath = abspath(pathname)

    def relpath(self, root):
        return relpath(self, root)

    def delete(self):
        print 'TODO: (FILE) actually delete things, once we are test-ready'
        #unlink(self.abspath)

class File(_FileLike):
    def __init__(self, pathname):
        super(File, self).__init__(pathname)

        assert isfile(pathname)


class Link(_FileLike):
    def __init__(self, pathname):
        super(Link, self).__init__(pathname)

        assert islink(pathname)

    def original(self, target=None):
        if target:
            symlink(target, self.abspath)
        return readlink(self)

###############################################################################
#
# Various Functions to make life great.
#
###############################################################################


